
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin) | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# My Emacs Configuration

- Omar Trejo
- May, 2015

This is my old Emacs configuration. Since July 2017 I've switched to using a
configuration based on Spacemacs. This configuration is left here for reference
purposes. If it's useful for you, feel free to use/adapt it.

Some repositories I relied on when building this setups were:

- [John Wiegley's Emacs configuration](https://github.com/jwiegley/dot-emacs)
- [Steve Purcell's Emacs configuration](https://github.com/purcell/emacs.d)
- [Aaron Bedra's Emacs configuration](http://aaronbedra.com/emacs.d/)
- [Bozhidar's Emacs configuration](https://github.com/bbatsov/emacs.d)
- [Emacs Prelude](https://github.com/bbatsov/prelude)
- [Awesome Emacs](https://github.com/emacs-tw/awesome-emacs)
- [Spacemacs](https://github.com/syl20bnr/spacemacs)

---

> "The best ideas are common property."
>
> —Seneca
